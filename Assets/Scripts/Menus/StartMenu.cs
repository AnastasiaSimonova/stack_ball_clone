﻿using UnityEngine;

public class StartMenu : MonoBehaviour
{
    public void SetVisible(bool value)
    {
        gameObject.SetActive(value);
    }

    public void OnStartMenuClick()
    {
        SetVisible(false);
    }
}
