﻿using UnityEngine;
using UnityEngine.UI;

public class GameOverMenu : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;
    [SerializeField] private ScoreManager scoreManager;

    [SerializeField] private GameObject scoreText;
    [SerializeField] private GameObject bestScoreText;

    public AdsManager adsManager;


    public void SetVisible(bool value)
    {
        gameObject.SetActive(value);

        scoreText.GetComponent<Text>().text = "Score:\n" + scoreManager.CurrentScore;
        bestScoreText.GetComponent<Text>().text = "Best\n" + scoreManager.BestScore;
    }

    public void OnRestartClick()
    {
        SetVisible(false);

        gameManager.Restart();
    }

    public void OnWatchAdsClick()
    {
        // TODO: Move to a separate method
        SetVisible(false);
        gameManager.ContinueGameForAds();

        adsManager.ShowRewardVideo();
    }
}
