﻿using UnityEngine;
using UnityEngine.UI;

public class LevelCompletedMenu : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;
    [SerializeField] private Text completedText;


    public void Show()
    {
        this.gameObject.SetActive(true);
        completedText.text = "Level " + gameManager.CurrentLevel + "\nCOMPLETED!";
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void OnContinueClick()
    {
        Hide();
        gameManager.StartNextLvl();
    }
}
