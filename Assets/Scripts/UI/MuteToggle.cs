using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(Toggle))]
public class MuteToggle : MonoBehaviour
{
    Toggle myToggle;

    private bool isSound;


    private void Start()
    {
        isSound = (Saves.IsSound() == 1) ? true : false;

        myToggle = GetComponent<Toggle>();

        myToggle.isOn = isSound;
        ApplyVolume(isSound);
    }

    public void ToggleAudioOnValueChange(bool audioOn)
    {
        isSound = audioOn;
        ApplyVolume(isSound);

        Saves.SaveIsSound(isSound == true ? 1 : 0);
    }

    private void ApplyVolume(bool audioOn)
    {
        if (audioOn)
            AudioListener.volume = 1;
        else
            AudioListener.volume = 0;
    }
}
