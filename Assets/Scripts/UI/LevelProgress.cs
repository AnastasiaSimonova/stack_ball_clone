﻿using UnityEngine;
using UnityEngine.UI;

public class LevelProgress : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;

    [SerializeField] private Slider slider;

    [SerializeField] private Text currentLevelText;
    [SerializeField] private Text nextLevelText;

    [SerializeField] private GameObject currentLevelImg;
    [SerializeField] private GameObject nextLevelImg;

    [SerializeField] private GameObject background;
    [SerializeField] private GameObject fill;

    private Color progressColor;
    

    public void UpdateData()
    {
        slider.value = 0;
        slider.maxValue = Saves.GetPlatformAmount();

        progressColor = FindObjectOfType<Ball>().GetComponent<MeshRenderer>().material.color;

        currentLevelImg.GetComponent<Image>().color = progressColor * new Color(progressColor.r * 0.5f, progressColor.g * 0.5f, progressColor.b * 0.5f);
        nextLevelImg.GetComponent<Image>().color = progressColor * new Color(progressColor.r * 0.5f, progressColor.g * 0.5f, progressColor.b * 0.5f);

        background.GetComponent<Image>().color = progressColor + Color.grey;
        fill.GetComponent<Image>().color = progressColor * new Color(progressColor.r * 0.7f, progressColor.g * 0.7f, progressColor.b * 0.7f);

        currentLevelText.GetComponent<Text>().text = Saves.GetCurrentLevel().ToString();
        nextLevelText.GetComponent<Text>().text = Saves.GetNextLevel().ToString();
    }

    public void AddValue(bool isAccelerated)
    {
        if (isAccelerated) slider.value += 2;
        else slider.value += 1;
    }

    private void Start()
    {
        UpdateData();
    }
}
