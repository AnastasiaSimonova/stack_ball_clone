using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Saves
{
    public static void ClearSaves()
    {
        PlayerPrefs.DeleteAll();
    }

    public static int GetPlatformAmount() => PlayerPrefs.GetInt("PlatformsAmount", 20);

    public static void SavePlatformsAmount(int value)
    {
        PlayerPrefs.SetInt("PlatformsAmount", value);
        Save();
    }

    public static int GetCurrentLevel() => PlayerPrefs.GetInt("CurrentLevel", 1);

    public static void SaveCurrentLevel(int value)
    {
        PlayerPrefs.SetInt("CurrentLevel", value);
        Save();
    }

    public static int GetNextLevel() => PlayerPrefs.GetInt("NextLevel", 2);

    public static void SaveNextLevel(int value)
    {
        PlayerPrefs.SetInt("NextLevel", value);
        Save();
    }

    public static int GetScore() => PlayerPrefs.GetInt("Score", 0);

    public static void SaveScore(int value)
    {
        PlayerPrefs.SetInt("Score", value);
        Save();
    }

    public static int GetBestScore() => PlayerPrefs.GetInt("BestScore", 0);

    public static void SaveBestScore(int value)
    {
        PlayerPrefs.SetInt("BestScore", value);
        Save();
    }

    public static int IsSound() => PlayerPrefs.GetInt("IsSound", 1);

    public static void SaveIsSound(int value)
    {
        PlayerPrefs.SetInt("IsSound", value);
        Save();
    }

    private static void Save()
    {
        PlayerPrefs.Save();
    }
}
