﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Ball : MonoBehaviour
{
    [SerializeField] private GameObject gameManager;
    [SerializeField] private ScoreManager scoreManager;
    [SerializeField] private ParticleSystem particles;
    [SerializeField] private LevelProgress levelProgress;

    [SerializeField] private GameObject splashEffect;

    [SerializeField] private float clickingSpeed = 3f;

    public AudioClip jumpClip, breakPlatformClip, dieClip, winClip;

    private float jumpSpeed = 2.6f;

    private Color[] colors = new Color[6] {Color.red, Color.green, Color.blue, Color.yellow, Color.cyan, Color.white};
    private Color color;

    private Rigidbody ballBody;

    private bool clicked = false;
    private bool isAccelerated = false;
    

    public void SetActive()
    {
        ballBody.isKinematic = false;
    }

    private void Awake()
    {
        gameObject.SetActive(true);
        ballBody = GetComponent<Rigidbody>();
        color = colors[Random.Range(0, colors.Length)];
        GetComponent<Renderer>().material.color = color;
    }


    private void Update()
    {
        // Prevent ball movement for UI clicking
        if (EventSystem.current.IsPointerOverGameObject()) return;

        if (Input.GetMouseButtonDown(0)) clicked = true;
        else if (Input.GetMouseButtonUp(0)) clicked = false;

        // Ball movement
        if (clicked) ballBody.velocity = Vector3.up * -clickingSpeed;
    }

    private void OnCollisionEnter(Collision collision)
    {
        // Check Win platform and win game
        if (collision.collider.gameObject.CompareTag("Win"))
        {
            // TODO: implement PlayWinParticles();
            gameManager.GetComponent<GameManager>().lvlCompletedMenu.Show();
            SoundManager.Instance.Play(winClip);
        }
        // Move ball to next platform
        if (clicked)
        {
            if (isAccelerated)
            {
                // Accelerated ball breaks any platform
                if (collision.collider.gameObject.CompareTag("Good") || collision.collider.gameObject.CompareTag("Bad"))
                {
                    Destroy(collision.collider.transform.parent.gameObject);
                    scoreManager.AddScore(isAccelerated);
                    levelProgress.AddValue(isAccelerated);

                    SoundManager.Instance.Play(breakPlatformClip);
                }
            }

            else
            {
                // Break Good platform and get score
                if (collision.collider.gameObject.CompareTag("Good"))
                {
                    Destroy(collision.collider.transform.parent.gameObject);
                    scoreManager.AddScore(isAccelerated);
                    levelProgress.AddValue(isAccelerated);

                    SoundManager.Instance.Play(breakPlatformClip);
                }
                // Break Bad platform and lose game
                else if (collision.collider.gameObject.CompareTag("Bad"))
                {
                    SoundManager.Instance.Play(dieClip);

                    clicked = false;
                    ballBody.isKinematic = true;

                    gameManager.GetComponent<GameManager>().GameOver();
                }
            }
        }
        // Bounce ball without breaking platforms
        else
        {
            ballBody.velocity = Vector3.up * jumpSpeed;

            if (!collision.collider.gameObject.CompareTag("Win"))
            {
                EmitSplash(collision);

                SoundManager.Instance.Play(jumpClip);
            }
        }
    }

    // TODO: implement this particles
    private void PlayWinParticles()
    {
        /*var winParticles = gameObject.GetComponentInChildren<ParticleSystem>();
        winParticles.gameObject.SetActive(true);
        winParticles.Play();*/
    }

    // Bouncing effect
    private void EmitSplash(Collision collision)
    {
        var splash = Instantiate(splashEffect);
        splash.transform.parent = collision.collider.transform;
        float randomScale = Random.Range(0.11f, 0.28f);
        splash.transform.localScale = new Vector3(randomScale, randomScale, 1);
        splash.transform.position =
                    new Vector3(transform.position.x, transform.position.y - 0.22f, transform.position.z);
        splash.GetComponent<Renderer>().material.color = color;
    }
}

