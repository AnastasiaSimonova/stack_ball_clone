﻿using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;

    [SerializeField] private GameObject ballPrefab;

    [SerializeField] private GameObject ball;
    [SerializeField] private GameObject[] platformPrefabs = new GameObject[4];
    [SerializeField] private GameObject winPlatform;

    [SerializeField] private float platformHeigth;

    private int platformsAmount;
    private float rotationSpeed = 40f;
    private float angleStep = 8f;

    private GameObject platformPrefab;

    private Color goodRandomMaterial;
    private Color badMaterialColor = Color.black;

    private int badMaterialProbability = 10;

    private readonly Color[] colors = new Color[6] { Color.red, Color.green, Color.blue, Color.yellow, Color.cyan, Color.white};


    public void GenerateLevel()
    {
        platformsAmount = Saves.GetPlatformAmount();

        ball.SetActive(true);
        mainCamera.transform.position = new Vector3(0f, 2f, -8.5f);
        goodRandomMaterial = colors[Random.Range(0, colors.Length)];
        platformPrefab = platformPrefabs[Random.Range(0, platformPrefabs.Length)];

        if (transform.childCount > 0) CleanPlatforms();

        for (int i = 0; i < platformsAmount; i++)
        {
            var newPlatform = Instantiate(platformPrefab, Vector3.up * -platformHeigth * i, Quaternion.Euler(0, angleStep * i, 0), transform);
            int childCount = newPlatform.transform.childCount;

            for (int j = childCount - 1; j >= 0; j--)
            {
                var child = newPlatform.transform.GetChild(j).gameObject;

                if (Random.Range(0, 100) > badMaterialProbability)
                {
                    child.GetComponent<Renderer>().material.color = goodRandomMaterial;
                    child.tag = "Good";
                }
                else
                {
                    child.GetComponent<Renderer>().material.color = badMaterialColor;
                    child.tag = "Bad";
                }
            }
        }
        Instantiate(winPlatform, Vector3.up * -platformsAmount * platformHeigth, Quaternion.identity, transform);
    }

    private void Update()
    {
        transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
    }

    private void CleanPlatforms()
    {
        int childCound = transform.childCount;
        for (int i = childCound - 1; i >= 0; i--)
        {
            DestroyImmediate(transform.GetChild(i).gameObject);
        }
    }
}
