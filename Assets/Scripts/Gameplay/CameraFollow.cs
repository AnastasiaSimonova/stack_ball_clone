﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform target;

    private float minY = 2f;
    private float offset;


    public void SetTarget(Transform obj)
    {
        target = obj;
    }

    private void Start()
    {
        offset = transform.position.y - target.position.y;
    }

    private void Update()
    {
        if (target.position.y < minY)
        {
            minY = target.position.y;
            transform.position = new Vector3(transform.position.x, minY + offset, transform.position.z);
        }
    }
}
