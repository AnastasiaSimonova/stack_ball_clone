using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class AdsManager : MonoBehaviour
{
    string BANNER_ID = "ca-app-pub-3940256099942544/6300978111"; // ��������
    //string BANNER_ID = "ca-app-pub-6360387929682183/7281980024"; // ������
    string INTERSTITIAL_ID = "ca-app-pub-3940256099942544/1033173712"; // ��������
    //string INTERSTITIAL_ID = "ca-app-pub-6360387929682183/4318094705"; // ������
    string REWARDED_VIDEO_ID = "ca-app-pub-3940256099942544/5224354917"; // ��������
    //string REWARDED_VIDEO_ID = "ca-app-pub-6360387929682183/7090408339"; // ������

    private BannerView bannerView;
    private InterstitialAd interstitial;
    private RewardBasedVideoAd rewardVideo;


    private void Start()
    {
        MobileAds.Initialize(initStatus => { });

        RequestBanner();
        RequestInterstitial();
        RequestRewardBasedVideo();
    }

    // BANNER
    public void RequestBanner()
    {
        Debug.Log("My RequestBanner");

        bannerView = new BannerView(BANNER_ID, AdSize.Banner, AdPosition.Bottom);

        // Called when an ad request has successfully loaded.
        bannerView.OnAdLoaded += this.HandleOnAdLoaded;
        // Called when an ad request failed to load.
        bannerView.OnAdFailedToLoad += this.HandleOnAdFailedToLoad;
        // Called when an ad is clicked.
        bannerView.OnAdOpening += this.HandleOnAdOpened;
        // Called when the user returned from the app after an ad click.
        bannerView.OnAdClosed += this.HandleOnAdClosed;
        // Called when the ad click caused the user to leave the application.
        bannerView.OnAdLeavingApplication += this.HandleOnAdLeavingApplication;

        AdRequest request = new AdRequest.Builder().Build();
        bannerView.LoadAd(request);
    }

    public void ShowBanner()
    {
        Debug.Log("My ShowBanner");

        AdRequest request = new AdRequest.Builder().Build();
        bannerView.LoadAd(request);
    }

    // INTERSTITIAL
    public void RequestInterstitial()
    {
        Debug.Log("My RequestInterstitial");

        interstitial = new InterstitialAd(INTERSTITIAL_ID);
        this.interstitial = new InterstitialAd(INTERSTITIAL_ID);

        // Called when an ad request has successfully loaded.
        interstitial.OnAdLoaded += HandleOnAdLoaded;
        // Called when an ad request failed to load.
        interstitial.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        // Called when an ad is shown.
        interstitial.OnAdOpening += HandleOnAdOpened;
        // Called when the ad is closed.
        interstitial.OnAdClosed += HandleOnAdClosed;
        // Called when the ad click caused the user to leave the application.
        interstitial.OnAdLeavingApplication += HandleOnAdLeavingApplication;

        AdRequest request = new AdRequest.Builder().Build();
        interstitial.LoadAd(request);
    }

    public void ShowInterstitial()
    {
        Debug.Log("My ShowInterstitial");

        if (interstitial.IsLoaded())
        {
            Debug.Log("My interstitial.IsLoaded");
            interstitial.Show();
        }
    }


    // REWARD VIDEO
    public void RequestRewardBasedVideo()
    {
        Debug.Log("My RequestRewardBasedVideo");

        rewardVideo = RewardBasedVideoAd.Instance;

        AdRequest request = new AdRequest.Builder().Build();
        rewardVideo.LoadAd(request, REWARDED_VIDEO_ID);
    }

    public void ShowRewardVideo()
    {
        Debug.Log("My ShowRewardVideo");

        if (rewardVideo.IsLoaded())
        {
            Debug.Log("My rewardVideo.IsLoaded");
            rewardVideo.Show();
        }
    }


    // FOR EVENTS AND DELEGATES FOR ADS
    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        Debug.Log("My HandleOnAdLoaded " + " " + sender + " " + args);

        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.Log("My HandleOnAdFailedToLoad " + " " + sender + " " + args.Message);

        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
        + args.Message);
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
        Debug.Log("My HandleOnAdOpened " + " " + sender + " " + args);

        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        Debug.Log("My HandleOnAdClosed " + " " + sender + " " + args);

        MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {
        Debug.Log("My HandleOnAdClosed " + " " + sender + " " + args);

        MonoBehaviour.print("HandleAdLeavingApplication event received");
    }
}
