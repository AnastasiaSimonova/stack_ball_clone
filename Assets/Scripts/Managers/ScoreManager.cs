﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] private Text scoreText;

    private const int ScoreStep = 2;

    public int CurrentScore { get; private set; }
    public int BestScore { get; private set; }


    public void AddScore(bool isAccelerated)
    {
        if (isAccelerated)
        {
            CurrentScore += ScoreStep;
        }
        else
        {
            CurrentScore++;
        }

        scoreText.text = CurrentScore.ToString();
        Saves.SaveScore(CurrentScore);
    }

    public void ResetScore()
    {
        CurrentScore = 0;
        scoreText.text = CurrentScore.ToString();
        Saves.SaveScore(CurrentScore);
    }

    public void UpdateBestScore()
    {
        if (CurrentScore > BestScore)
        {
            BestScore = CurrentScore;
            Saves.SaveBestScore(BestScore);
        }
    }


    private void Awake()
    {
        CurrentScore = Saves.GetScore();
        BestScore = Saves.GetBestScore();
    }

    private void Start()
    {
        scoreText.text = CurrentScore.ToString();
    }
}
