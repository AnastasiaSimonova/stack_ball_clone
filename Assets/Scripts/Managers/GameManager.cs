﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private LevelGenerator lvlGenerator;

    [SerializeField] private GameObject startMenu;
    [SerializeField] private GameObject gameOverMenu;
    [SerializeField] private ScoreManager scoreManager;

    public LevelCompletedMenu lvlCompletedMenu;

    public int CurrentLevel { get; set; }
    public int NextLevel { get; set; }


    public void GameOver()
    {
        scoreManager.UpdateBestScore();
        gameOverMenu.GetComponent<GameOverMenu>().SetVisible(true);
    }

    public void StartNextLvl()
    {
        IncreaseLevel();
        SceneManager.LoadScene(0);
        startMenu.GetComponent<StartMenu>().SetVisible(false);
    }

    public void Restart()
    {
        scoreManager.GetComponent<ScoreManager>().ResetScore();
        SceneManager.LoadScene(0);
        startMenu.GetComponent<StartMenu>().SetVisible(true);
    }

    public void ContinueGameForAds()
    {
        GameObject.FindGameObjectWithTag("Ball").GetComponent<Ball>().SetActive();
    }

    private void Start()
    {
        CurrentLevel = Saves.GetCurrentLevel();
        NextLevel = Saves.GetNextLevel();

        StartGame();
    }

    private void IncreaseLevel()
    {
        CurrentLevel++;
        NextLevel = CurrentLevel + 1;

        Saves.SaveCurrentLevel(CurrentLevel);
        Saves.SaveNextLevel(NextLevel);

        Saves.SavePlatformsAmount(Saves.GetPlatformAmount() * 2);
    }

    private void StartGame()
    {
        lvlGenerator.GenerateLevel();
    }
}
