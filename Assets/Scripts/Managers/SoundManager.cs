﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private bool soundPlay = true;
	public static SoundManager Instance = null;

	public AudioSource SoundSource;


	public void Play(AudioClip clip)
	{
		SoundSource.PlayOneShot(clip);
	}

	public void SoundOnOff()
    {
        soundPlay = !soundPlay;
    }
	
	private void Awake()
	{
		if (Instance == null) Instance = this;
		else if (Instance != this) Destroy(gameObject);

		//DontDestroyOnLoad(gameObject);
	}
}
